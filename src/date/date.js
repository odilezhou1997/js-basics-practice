function getDayOfMonth (dateStr) {
  const day = new Date(dateStr);
  return day.getDate();
}

function getDayOfWeek (dateStr) {
  const day = new Date(dateStr);
  return day.getDay();
}

function getYear (dateStr) {
  const day = new Date(dateStr);
  return day.getFullYear();
}

function getMonth (dateStr) {
  const day = new Date(dateStr);
  return day.getMonth();
}

function getMilliseconds (dateStr) {
  const day = new Date(dateStr);
  return day.getTime(dateStr);
}

export { getDayOfMonth, getDayOfWeek, getYear, getMonth, getMilliseconds };
