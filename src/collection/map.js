function doubleItem (collection) {
  return collection.map(item => item * 2);
}

function doubleEvenItem (collection) {
  return collection.filter(number => number % 2 === 0).map(item => item * 2);
}

function covertToCharArray (collection) {
  return collection.map(item => String.fromCharCode(item + 96));
}

function getOneClassScoreByASC (collection) {
  return collection.filter(item => item.class === 1).map(item => item.score).sort();
}

export { doubleItem, doubleEvenItem, covertToCharArray, getOneClassScoreByASC };
