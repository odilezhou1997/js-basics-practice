function getAllEvens (collection) {
  return collection.filter(item => item % 2 === 0);
}

function getAllIncrementEvens (start, end) {
  const array = [];
  for (let number = start; number <= end; number++) {
    array.push(number);
  }
  return array.filter(item => item % 2 === 0);
}

function getIntersectionOfcollections (collection1, collection2) {
  return collection1.filter(item => collection2.includes(item));
}

function getUnionOfcollections (collection1, collection2) {
  return Array.from(new Set(collection1.concat(collection2)));
}

function countItems (collection) {
  return collection.reduce((prev, next) => {
    prev[next] = (prev[next] + 1) || 1;
    return prev;
  }, {});
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};
