function getMaxNumber (collction) {
  return collction.reduce((prev, next) => prev > next ? prev : next);
}

function isSameCollection (collction1, collction2) {
  return collction1.reduce((accumulator, cur, index) =>
    cur === collction2[index] && collction1.length === collction2.length ? true : !true);
}

function sum (collction) {
  return collction.reduce((total, number) => total + number, 0);
}

function computeAverage (collction) {
  return collction.reduce((total, number) => (total + number), 0) / collction.length;
}

function lastEven (collction) {
  return collction.reduce((prev, next) => next % 2 === 0 ? next : prev);
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };
