function flatArray (arr) {
  return arr.reduce((prev, next) => prev.concat(next), []);
}

function aggregateArray (arr) {
  return arr.map(item => [item * 2]);
}

function getEnumerableProperties (obj) {
  return Object.keys(obj);
}

function removeDuplicateItems (arr) {
  return Array.from(new Set(arr));
}

function removeDuplicateChar (str) {
  return Array.from(new Set([...str])).join('');
}

function addItemToSet (set, item) {
  set.add(item);
  return set;
}

function removeItemToSet (set, item) {
  set.delete(item);
  return set;
}

function countItems (arr) {
  const newArray = new Map();
  Array.from(new Set(arr)).map(key => newArray.set(key, arr.filter(value => value === key).length));
  return newArray;
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};
