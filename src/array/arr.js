function joinArrays (arr1, arr2) {
  return arr1.concat(arr2);
}

function checkAdult (arr, age) {
  let result;
  const newArray = arr.filter(item => item >= age);
  if (arr.length !== newArray.length) {
    result = false;
    return result;
  }
  result = true;
  return result;
}

function findAdult (arr, age) {
  const newArray = arr.filter(item => item >= age);
  return newArray[0];
}

function convertArrToStr (arr, str) {
  const newArray = arr.join(str);
  return newArray;
}

function removeLastEle (arr) {
  arr.pop();
  return arr;
}

function addNewItem (arr, item) {
  arr.push(item);
  return arr;
}

function removeFirstItem (arr) {
  arr.shift();
  return arr;
}

function addNewItemToBeginArr (arr, item) {
  arr.unshift(item);
  return arr;
}

function reverseOrder (arr) {
  arr.reverse();
  return arr;
}

function selectElements (arr, start, end) {
  const newArray = arr.slice(start, end);
  return newArray;
}

function addItemsToArray (arr, index, howmany, item) {
  arr.splice(index, howmany, item);
  return arr;
}

function sortASC (arr) {
  arr.sort((a, b) => a - b);
  return arr;
}

function sortDESC (arr) {
  arr.sort((a, b) => b - a);
  return arr;
}

export {
  joinArrays,
  checkAdult,
  findAdult,
  convertArrToStr,
  removeLastEle,
  addNewItem,
  removeFirstItem,
  addNewItemToBeginArr,
  reverseOrder,
  selectElements,
  addItemsToArray,
  sortASC,
  sortDESC
};
